
#include <iostream>
#include <stdexcept>
#include <string>
#include <memory>
#include <functional>
#include <vector>

#include <imgui.h>
#include "imgui_impl_sdl_gl3.h"
#include <GL/gl3w.h>
#include <SDL.h>



namespace sys
{

    struct Context
    {
        Context( const Context & ) = delete;
        Context operator=( const Context & ) = delete;

        Context()
        {
            // Setup SDL
            if( SDL_Init( SDL_INIT_EVERYTHING ) != 0 )
            {
                throw std::runtime_error( std::string( "SDL initialization failed: Error: " ) + SDL_GetError() );
            }
        }

        ~Context()
        {
            SDL_Quit();
        }
    };

    struct WindowSize
    {
        WindowSize() = default;
        WindowSize( int width, int height )
            : width( width ), height( height )
        {}

        int width = 0;
        int height = 0;
    };

    struct WindowState
    {
        WindowSize size;
        std::string title;
    };

    class Window
    {

    public:
        struct Event
        {
            SDL_Event sdl_event;
        };

        using UpdateTask = std::function<void( Event& event )>;

        Window( const Window& ) = delete;
        Window& operator=( const Window& ) = delete;

        explicit Window( Context& context, WindowState initial_state )
            : m_window( SDL_CreateWindow( initial_state.title.c_str()
                , SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED
                , initial_state.size.width, initial_state.size.height
                , SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE )
                )
            , m_window_desc( std::move( initial_state ) )
        {
            if( !m_window )
            {
                throw std::runtime_error( "Failed to create SDL window" );
            }
        }

        ~Window()
        {
            SDL_DestroyWindow( m_window );
        }

        operator SDL_Window* () { return m_window; }

        template< class Func >
        int add_update_task( Func&& func )
        {
            m_update_tasks.push_back( std::forward<Func>( func ) );
            return m_update_tasks.size() - 1;
        }

        void remove_update_task( int task_id )
        {
            m_update_tasks[ task_id ] = {};
        }

        // return true until an exist request is done
        bool update()
        {
            Event event;
            while( SDL_PollEvent( &event.sdl_event ) )
            {
                if( event.sdl_event.type == SDL_QUIT )
                    return false;

                for( auto&& task : m_update_tasks )
                {
                    if( task )
                        task( event );
                }
            }
            return true;
        }

        void display()
        {
            SDL_GL_SwapWindow( m_window );
        }

    private:
        SDL_Window* m_window;
        std::vector<UpdateTask> m_update_tasks;
        WindowState m_window_desc;
    };

    class GraphicContext
    {
    public:

        GraphicContext( const GraphicContext & ) = delete;
        GraphicContext operator=( const GraphicContext & ) = delete;

        explicit GraphicContext( Window& window )
        {
            SDL_GL_SetAttribute( SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG );
            SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE );
            SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );
            SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 24 );
            SDL_GL_SetAttribute( SDL_GL_STENCIL_SIZE, 8 );
            SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, 3 );
            SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, 2 );
            SDL_DisplayMode current;
            SDL_GetCurrentDisplayMode( 0, &current );

            m_gl_context = SDL_GL_CreateContext( window );
            gl3wInit();
        }

        ~GraphicContext()
        {
            SDL_GL_DeleteContext( m_gl_context );
        }

        operator SDL_GLContext& () { return m_gl_context; }

        void update()
        {
            glViewport( 0, 0, (int)ImGui::GetIO().DisplaySize.x, (int)ImGui::GetIO().DisplaySize.y );
        }

        void clear( const ImVec4& clear_color )
        {
            glClearColor( clear_color.x, clear_color.y, clear_color.z, clear_color.w );
            glClear( GL_COLOR_BUFFER_BIT );
        }

    private:

        SDL_GLContext m_gl_context;
    };

}

namespace ui
{
    template< class T >
    auto display_ui( T&& element )
    {
        std::forward<T>(element).display_ui();
    }

    class Element
    {
    public:

        template< class T, class = typename std::enable_if< !std::is_same< typename std::decay<T>::type, Element >::value >::type >
        Element( T&& object )
            : m_store( std::make_unique<Model<T>>( std::forward<T>( object ) ) )
        {
        }

        template< class T, class = typename std::enable_if< !std::is_same< typename std::decay<T>::type, Element >::value >::type >
        Element( T* object_ptr )
            : m_store( std::make_unique<ModelPtr<T>>( object_ptr ) )
        {
        }

        Element( const Element& ) = default;
        Element& operator=( const Element& ) = default;
        Element( Element&& ) = default;
        Element& operator=( Element&& ) = default;

        void display()
        {
            m_store->ui_display();
        }


    private:
        // The following is a naive sub-optimal but working way to do correct value-semantic+entity-semantic type erasure.
        struct Concept
        {
            virtual void ui_display() = 0;
            ~Concept() = default;
        };

        template< class T >
        struct Model : Concept
        {
            T object;

            explicit Model( T object ) : object( std::move( object ) ) {}

            void ui_display() override
            {
                display_ui( object );
            }

        };

        template< class T >
        struct ModelPtr : Concept
        {
            T* object;

            explicit ModelPtr( T* object ) : object( std::move( object ) ) {}

            void ui_display() override
            {
                display_ui( *object );
            }

        };

        std::unique_ptr<Concept> m_store;
    };

    void display_ui( Element& element )
    {
        element.display();
    }

    struct ElementGroup
    {
        std::string name;
        std::vector<Element> elements;

        template< class T >
        void add( T&& element )
        {
            elements.push_back( std::forward<T>( element ) );
        }
    };

    void display_ui( ElementGroup& group )
    {
        ImGui::Separator();
        ImGui::BeginGroup();
        ImGui::LabelText( "", "-- %s --", group.name.c_str() );
        for( auto& element : group.elements )
        {
            display_ui( element );
        }
        ImGui::EndGroup();
    }

    class Context
    {
    public:
        Context( const Context& ) = delete;
        Context& operator=( const Context& ) = delete;

        explicit Context( sys::Window& window )
            : m_window( window )
        {
            // Setup ImGui binding
            if( !ImGui_ImplSdlGL3_Init( m_window ) )
            {
                throw std::runtime_error( "IMGUI SDL2 GL3 Initalization failed!" );
            }

            // Load Fonts
            // (there is a default font, this is only if you want to change it. see extra_fonts/README.txt for more details)
            //ImGuiIO& io = ImGui::GetIO();
            //io.Fonts->AddFontDefault();
            //io.Fonts->AddFontFromFileTTF("../../extra_fonts/Cousine-Regular.ttf", 15.0f);
            //io.Fonts->AddFontFromFileTTF("../../extra_fonts/DroidSans.ttf", 16.0f);
            //io.Fonts->AddFontFromFileTTF("../../extra_fonts/ProggyClean.ttf", 13.0f);
            //io.Fonts->AddFontFromFileTTF("../../extra_fonts/ProggyTiny.ttf", 10.0f);
            //io.Fonts->AddFontFromFileTTF("c:\\Windows\\Fonts\\ArialUni.ttf", 18.0f, NULL, io.Fonts->GetGlyphRangesJapanese());

            update_task_id = window.add_update_task( [this]( sys::Window::Event& event ) {
                ImGui_ImplSdlGL3_ProcessEvent( &event.sdl_event );
            } );
        }

        ~Context()
        {
            ImGui_ImplSdlGL3_Shutdown();
            m_window.remove_update_task( update_task_id );
        }

        template< class T >
        void add( T&& new_element )
        {
            m_elements.emplace_back( std::forward<T>( new_element ) );
        }

        void display()
        {
            ImGui_ImplSdlGL3_NewFrame();

            for( auto& element : m_elements )
            {
                display_ui( element );
            }

            // Rendering
            ImGui::Render();
        }

    private:
        sys::Window& m_window;
        std::vector<Element> m_elements;
        int update_task_id = -1;
    };

}

namespace my
{
    struct HelloWorld
    {
        bool show_test_window = false;
        bool show_another_window = false;
        ImVec4 clear_color = ImColor( 114, 144, 154 );
        float f = 0.0f;
    };

    void display_ui( HelloWorld& hello_world )
    {
        // 1. Show a simple window
        // Tip: if we don't call ImGui::Begin()/ImGui::End() the widgets appears in a window automatically called "Debug"
        {
            ImGui::Text( "Hello, world!" );
            ImGui::SliderFloat( "float", &hello_world.f, 0.0f, 1.0f );
            ImGui::ColorEdit3( "clear color", (float*)&hello_world.clear_color );
            if( ImGui::Button( "Test Window" ) ) hello_world.show_test_window ^= 1;
            if( ImGui::Button( "Another Window" ) ) hello_world.show_another_window ^= 1;
            ImGui::Text( "Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate );
        }

        // 2. Show another simple window, this time using an explicit Begin/End pair
        if( hello_world.show_another_window )
        {
            ImGui::SetNextWindowSize( ImVec2( 200, 100 ), ImGuiSetCond_FirstUseEver );
            ImGui::Begin( "Another Window", &hello_world.show_another_window );
            ImGui::Text( "Hello" );
            ImGui::End();
        }

        // 3. Show the ImGui test window. Most of the sample code is in ImGui::ShowTestWindow()
        if( hello_world.show_test_window )
        {
            ImGui::SetNextWindowPos( ImVec2( 650, 20 ), ImGuiSetCond_FirstUseEver );
            ImGui::ShowTestWindow( &hello_world.show_test_window );
        }
    }

    struct Lol
    {
        std::string title;
        std::string text;
        bool is_visible;

        void display_ui()
        {
            if( is_visible )
            {
                ImGui::Begin( title.c_str(), &is_visible );
                ImGui::SetNextWindowSize( ImVec2( 200, 100 ) );
                ImGui::Text( text.c_str() );
                ImGui::End();
            }
        }

    };


    struct Block
    {
        static int next_id()
        {
            static int next_id = 0;
            return next_id++;
        }

        std::string text = std::to_string( next_id() ) ;
        bool is_on = false;

        void display_ui()
        {
            ImGui::Checkbox( text.c_str(), &is_on );
            if( is_on )
            {
                ImGui::SameLine();
                ImGui::Text( "ON" );
            }
        }
    };
}


int main( int, char** )
{
    using namespace std::literals;

    static const sys::WindowState WINDOW_SETUP {
        { 1280, 800 }, "Hello, Componentized World!"
    };

    sys::Context system_context;
    sys::Window window { system_context, WINDOW_SETUP };
    sys::GraphicContext graphic_context { window };
    ui::Context ui_context { window };

    my::HelloWorld hello_world;

    ui_context.add( &hello_world );
    ui_context.add( my::Lol{ "LOL"s, "Yeah"s, true } );
    ui_context.add( my::Lol{ "KIKOO"s, "Like A Boss"s, true } );
    ui_context.add( my::Lol{ "WTF????"s, "WTF"s, false } );

    for( int i = 0; i < 10; ++i )
    {
        ui::ElementGroup blocks{ std::to_string( i ) };
        for( int x = 0; x < 10; ++x )
        {
            blocks.add( my::Block{} );
        }
        ui_context.add( std::move( blocks ) );
    }

    // Main loop
    while( window.update() )
    {
        graphic_context.update();
        graphic_context.clear( hello_world.clear_color );
        ui_context.display();
        window.display();
    }

    // Cleanup

    return 0;
}
